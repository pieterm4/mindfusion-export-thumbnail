﻿using System.Threading.Tasks;
using MindFusion.Diagramming.Wpf;

namespace ExportPNG.Services
{
    public interface IThumbnailFactory
    {
        Task SaveThumbnail(Diagram diagram);
    }
}