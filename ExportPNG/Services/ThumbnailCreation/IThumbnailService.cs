﻿using System.Drawing;
using System.Threading.Tasks;

namespace ExportPNG.Services.ThumbnailCreation
{
    public interface IThumbnailService
    {
        float ImageResolution { get; set; }
        Task<Bitmap> ResizeImage(Bitmap image, int maxWidth, int maxHeight);
    }
}