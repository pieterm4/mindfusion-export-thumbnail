﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using ExportPNG.Converters;
using ExportPNG.Services.ExportToPNG;
using ExportPNG.Services.ThumbnailCreation;
using MindFusion.Diagramming.Wpf;

namespace ExportPNG.Services
{
    public class ThumbnailFactory : IThumbnailFactory
    {
        private readonly string _filePath;

        public ThumbnailFactory()
        {
            _filePath = GetDirectoryPath();
        }

        private const int MaxHeight = 600;

        private const int MaxWidth = 700;

        public async Task SaveThumbnail(Diagram diagram)
        {
            var image = await CreateImage(diagram);
            var thumbnailService = new ThumbnailService();
            var bitmap = BitmapConverter.BitmapFromSource(image);
            var thumb = await thumbnailService.ResizeImage(bitmap, MaxWidth, MaxHeight);
            var bitmapSource = BitmapConverter.SourceFromBitmap(thumb);
            var exportService = new ExportToPngService(bitmapSource, _filePath, diagram.Name);

            await exportService.ExportToPNG();
        }

        private async Task<BitmapSource> CreateImage(Diagram diagram)
        {
            return await Task.Run(() =>
            {
                BitmapSource img = null;
                Application.Current.Dispatcher.Invoke(() => img = diagram.CreateImage());
                return img;
            });
        }

        private string GetDirectoryPath()
        {
            var parent =
                System.IO.Directory.GetParent((System.IO.Directory.GetParent(Environment.CurrentDirectory).ToString()));
            return $"{parent}\\ExportedImage\\";
        }
    }
}