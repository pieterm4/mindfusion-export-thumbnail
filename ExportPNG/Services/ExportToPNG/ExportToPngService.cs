﻿using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ExportPNG.Services.ExportToPNG
{
    public class ExportToPngService : IExportToPngService
    {
        private readonly BitmapSource _image;
        private string _filePath;

        public ExportToPngService(BitmapSource image, string filePath, string diagramName)
        {
            _image = image;
            SetCorrectFilePath(filePath, diagramName);
        }

        public async Task ExportToPNG()
        {
            await Task.Run(() =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    using (var fileStream = new FileStream(_filePath, FileMode.Create))
                    {
                        var encoder = ConvertToPng(_image);
                        encoder.Save(fileStream);
                    }
                });

            });

        }

        private static PngBitmapEncoder ConvertToPng(BitmapSource img)
        {
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(img));
            return encoder;
        }

        private void SetCorrectFilePath(string filePath, string diagramName)
        {
            _filePath = filePath + $"{diagramName}.png";
        }
    }
}