﻿using System.Threading.Tasks;

namespace ExportPNG.Services.ExportToPNG
{
    public interface IExportToPngService
    {
        Task ExportToPNG();
    }
}