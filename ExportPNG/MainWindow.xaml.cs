﻿using System.Diagnostics;
using System.Linq;
using System.Windows;
using ExportPNG.Services;
using MindFusion.Diagramming.Wpf;

namespace ExportPNG
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            shapeList.ItemsSource = Shape.Shapes
                .Cast<Shape>()
                .Select(s => new ShapeNode
                {
                    Shape = s,
                    Bounds = new Rect(0, 0, 40, 40)
                });
        }

        private async void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            var stopwatch = new Stopwatch();

            stopwatch.Start();
            var thumbnailFactory = new ThumbnailFactory();
            await thumbnailFactory.SaveThumbnail(flowChart);
            stopwatch.Stop();

            Debug.WriteLine($"Elapsed time {stopwatch.Elapsed}");

            MessageBox.Show("Thumbnail has been exported", "Information", MessageBoxButton.OK);
            
        }
    }
}
